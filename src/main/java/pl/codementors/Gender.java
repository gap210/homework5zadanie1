package pl.codementors;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name ="genders")
public class Gender {

    public Gender() {
    }

    public Gender(GenderType gender) {
        this.gender = gender;
    }

    public enum GenderType {

        MALE,
        FEMALE;
    }


    @Id
    @Enumerated(EnumType.STRING)
    private GenderType gender;

    @Override
    public String toString() {
        return gender.name();
    }

    public GenderType getGender() {
        return gender;
    }

    public void setGender(GenderType gender) {
        this.gender = gender;
    }
}
