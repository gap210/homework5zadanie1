package pl.codementors;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "interests")
public class Interest {

    public Interest() {
    }

    public Interest(String name) {
        this.name = name;
    }

    @Id
    @GeneratedValue (strategy = GenerationType.IDENTITY)
    public int id;

    @Column
    private String name;

    @ManyToMany (cascade = {CascadeType.MERGE, CascadeType.PERSIST},mappedBy = "interests")
    private Set<Person> persons;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
