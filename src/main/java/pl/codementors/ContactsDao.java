package pl.codementors;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.swing.text.TableView;
import java.util.List;

public class ContactsDao {


    public Person addPerson(Person person) {
        EntityManager em = PersonsEntityManagerFactory.createEntityManager();
        EntityTransaction tx = em.getTransaction();
        tx.begin();
        em.persist(person);
        tx.commit();
        em.close();
        return person;
    }

    public List<Person> findAllPersons () {
        EntityManager em = PersonsEntityManagerFactory.createEntityManager();
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Person> query = cb.createQuery(Person.class);
        query.from(Person.class);
        List<Person> personList = em.createQuery(query).getResultList();
        em.close();
        return personList;
    }

    public Person updatePerson(Person person) {
        EntityManager em = PersonsEntityManagerFactory.createEntityManager();
        EntityTransaction tx = em.getTransaction();
        tx.begin();
        em.merge(person);
        tx.commit();
        em.close();
        return person;
    }

    public void removePerson (int id) {
        EntityManager em = PersonsEntityManagerFactory.createEntityManager();
        EntityTransaction tx = em.getTransaction();
        tx.begin();
        Person person = em.find(Person.class, id);
        em.remove(updatePerson(person));
        tx.commit();
        em.close();
    }

    public Interest addInterest (Interest interest) {
        EntityManager em = PersonsEntityManagerFactory.createEntityManager();
        EntityTransaction tx = em.getTransaction();
        tx.begin();
        em.persist(interest);
        tx.commit();
        em.close();
        return interest;
    }

    public List<Interest> findAllInterests () {
        EntityManager em = PersonsEntityManagerFactory.createEntityManager();
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Interest> query = cb.createQuery(Interest.class);
        query.from(Interest.class);
        List<Interest> interests = em.createQuery(query).getResultList();
        em.close();
        return interests;
    }

    public List<Gender> findAllGenders() {
        EntityManager em = PersonsEntityManagerFactory.createEntityManager();
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Gender> query = cb.createQuery(Gender.class);
        query.from(Gender.class);
        List<Gender> genderList = em.createQuery(query).getResultList();
        em.close();
        return genderList;
    }





}
