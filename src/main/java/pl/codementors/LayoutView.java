package pl.codementors;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;

import java.io.IOException;
import java.net.URL;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;

public class LayoutView implements Initializable {


    private Person person;

    private List<Person> persons;

    @FXML
    private TableView<Person> personData;

    @FXML
    private TableColumn<Person, String> first_name;

    @FXML
    private TableColumn<Person,String> last_name;

    @FXML
    private Button addButton;

    @FXML
    private Button removeButton;

    @FXML
    private TextField nameField;

    @FXML
    private TextField surnameField;

    @FXML
    private TextField phoneNumberField;

    @FXML
    private TextField genderType;

    @FXML
    private ListView<Interest> interestListView;



    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        first_name.setCellValueFactory(new PropertyValueFactory<>("first_name"));
        last_name.setCellValueFactory(new PropertyValueFactory<>("last_name"));



        Person person1 = new Person("Grzegorz", "Grzegorzewski", 123456789, new Gender(Gender.GenderType.MALE));
//        Person person2 = new Person("Michal", "Michalowski", 987654321, new Gender(Gender.GenderType.MALE));

//        dao.addPerson(person1);
//        dao.addPerson(person2);


        populateTableView();

//        personData.setOnMouseClicked((MouseEvent event) -> { //this functionality is now in FXML
//            if (event.getClickCount() > 0) {
//                onTableViewClicked();
//            }
//        });
    }

    public void onTableViewClicked () {
        if(personData.getSelectionModel().getSelectedItem() != null) {
            Person selectedPerson = personData.getSelectionModel().getSelectedItem();
            nameField.setText(selectedPerson.getFirst_name());
            surnameField.setText(selectedPerson.getLast_name());
            phoneNumberField.setText(String.valueOf(selectedPerson.getPhone_number()));
            genderType.setText(selectedPerson.getGender().toString());
        }
    }
    public void removeClickedPerson () {
        ContactsDao dao = new ContactsDao();
        if(personData.getSelectionModel().getSelectedItem() != null) {
            Person selectedPerson = personData.getSelectionModel().getSelectedItem();
            dao.removePerson(selectedPerson.getId());
            populateTableView();
            clearUndeitableView();
        }
    }
    public void clearUndeitableView () {
        nameField.clear();
        surnameField.clear();
        phoneNumberField.clear();
        genderType.clear();
    }

    public void populateTableView () {
        ContactsDao dao = new ContactsDao();
        persons = dao.findAllPersons();
        ObservableList<Person> observablePersons = FXCollections.observableArrayList();
        observablePersons.addAll(persons);
        personData.setItems(observablePersons);
    }

    public void onAddUserButtonClick () throws IOException {
        ContactsDao dao = new ContactsDao();
        boolean running = true;
        do {

            TextInputDialog input = new TextInputDialog("");
            input.setTitle("Podaj imie");
            input.setHeaderText(null);
            input.setContentText("Podaj imie: ");
            nameField.setText(input.showAndWait().orElse(""));
            running = false;

        } while (running);

        TextInputDialog input = new TextInputDialog("");
        input.setTitle("Podaj nazwisko");
        input.setHeaderText(null);
        input.setContentText("Podaj nazwisko: ");
        surnameField.setText(input.showAndWait().orElse(""));

        List<Gender.GenderType> genderChoices = Arrays.asList(Gender.GenderType.MALE, Gender.GenderType.FEMALE);
        ChoiceDialog<Gender.GenderType> genderBox = new ChoiceDialog<>(null, genderChoices);
        genderBox.setTitle("Plec");
        genderBox.setHeaderText(null);
        genderBox.setContentText("Podaj plec");
        genderType.setText(String.valueOf(genderBox.showAndWait().orElse(null)));

        TextInputDialog input1 = new TextInputDialog("");
        input1.setTitle("Numer telefonu");
        input1.setHeaderText(null);
        input1.setContentText("Podaj numer telefonu: ");
        phoneNumberField.setText(input1.showAndWait().orElse(""));

        Alert alert = new Alert(Alert.AlertType.INFORMATION, "Do you want to add a Person?");
        alert.setTitle("Adding Person");
        alert.setHeaderText("Name: " + nameField.getText() + "\n" + "Surname: " + surnameField.getText()
        + "\n" + "Gender: " + genderType.getText() + "\n" + "Phone number: " + phoneNumberField.getText());
        Optional<ButtonType> result = alert.showAndWait();
        if (result.get() == ButtonType.OK) {
            String gender = genderType.getText();
            Gender gender1 = new Gender(Gender.GenderType.valueOf(gender));
            Person person = new Person(nameField.getText(), surnameField.getText(), Integer.valueOf(phoneNumberField.getText()), gender1);
            dao.addPerson(person);
            populateTableView();
        }



    }



    public void onAddInterestButtonClick() throws IOException {
        ContactsDao dao = new ContactsDao();

        TextInputDialog input = new TextInputDialog("");
        input.setTitle("Zainteresowania");
        input.setHeaderText(null);
        input.setContentText("Podaj zaintresowanie: ");
        String interestName = input.showAndWait().orElse("");
        Interest newInterest = new Interest(interestName);
        dao.addInterest(newInterest);
    }
}



