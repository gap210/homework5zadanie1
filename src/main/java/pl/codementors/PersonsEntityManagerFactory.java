package pl.codementors;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class PersonsEntityManagerFactory {

    private static final EntityManagerFactory emf = Persistence.createEntityManagerFactory("contactsPU");

    public static final EntityManager createEntityManager() {
        return emf.createEntityManager();
    }

    public static void close() {
        emf.close();
    }
}
