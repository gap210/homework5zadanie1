package pl.codementors;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "persons")
public class Person {
    public Person() {
    }

    public Person(String first_name, String last_name, int phone_number, Gender gender) {
        this.first_name = first_name;
        this.last_name = last_name;
        this.phone_number = phone_number;
        this.gender = gender;
    }


    @Id
    @GeneratedValue (strategy = GenerationType.IDENTITY)
    private int id;


    @ManyToOne
    @JoinColumn(name = "gender", referencedColumnName = "gender")
    private Gender gender;

    @Column
    private String first_name;

    @Column
    private String last_name;

    @Column
    private int phone_number;


    @ManyToMany (cascade = {CascadeType.MERGE, CascadeType.PERSIST}, fetch = FetchType.EAGER)
    @JoinTable (name = "persons_interests", joinColumns = @JoinColumn (name = "person_id"), inverseJoinColumns = @JoinColumn(name = "interest_id"))
    private Set<Interest> interests;

    public String getFirst_name() {

        return first_name;
    }

    public void setFirst_name(String first_name) {

        this.first_name = first_name;
    }

    public String getLast_name() {

        return last_name;
    }

    public void setLast_name(String last_name) {

        this.last_name = last_name;
    }

    public int getPhone_number() {

        return phone_number;
    }

    public void setPhone_number(int phone_number) {

        this.phone_number = phone_number;
    }

    public Gender getGender() {

        return gender;
    }

    public void setGender(Gender gender) {

        this.gender = gender;
    }

    public int getId() {
        return id;
    }
}
