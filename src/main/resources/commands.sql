CREATE DATABASE contacts;
CREATE USER admin IDENTIFIED BY 'password';
GRANT ALL ON contacts.* TO admin;

CREATE TABLE genders (
gender VARCHAR(256)
PRIMARY KEY (gender)
);

CREATE TABLE persons (
id INT NOT NULL AUTO_INCREMENT,
first_name VARCHAR(256) NOT NULL,
last_name VARCHAR(256) NOT NULL,
phone_number INT NOT NULL,
gender INT NOT NULL,
FOREIGN KEY gender REFERENCES gender (gender),
PRIMARY KEY (id)
);

CREATE TABLE interests (
id INT NOT NULL AUTO_INCREMENT,
name VARCHAR(256) NOT NULL,
PRIMARY KEY (id)
);

CREATE TABLE persons_interests (
person_id INT NOT NULL,
interest_id INT NOT NULL,
PRIMARY KEY (person_id, interest_id),
FOREIGN KEY (person_id) REFERENCES persons (id),
FOREIGN KEY (interest_id) REFERENCES interests (id)
);

ALTER TABLE persons ADD FOREIGN KEY (gender_id) REFERENCES genders (id);

INSERT INTO genders (gender) VALUE ('Male');
INSERT INTO genders (gender) VALUE ('Female');

